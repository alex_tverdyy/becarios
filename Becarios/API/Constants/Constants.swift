//
//  Constants.swift
//  Becarios
//
//  Created by Alex Tverdyy on 11/07/2019.
//  Copyright © 2019 Alex Tverdyy. All rights reserved.
//

import Foundation

struct Constants {
    static let API_BASE = "https://guadaltech-fullstack.herokuapp.com/api"
    static let BECARIOS = "/becarios"
}
