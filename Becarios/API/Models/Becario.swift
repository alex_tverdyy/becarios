//
//  Becario.swift
//  Becarios
//
//  Created by Alex Tverdyy on 11/07/2019.
//  Copyright © 2019 Alex Tverdyy. All rights reserved.
//

import Foundation

struct Becario: Codable {
    let id: String
    let index: Int
    let guid: String
    let isActive: Bool
    let balance: String
    let picture: String
    let age: Int
    let eyeColor, name, gender, company: String
    let email, phone, address, about: String
    let registered: String
    let latitude, longitude: Double
    let tags: [String]
    let friends: [Friend]
    let greeting, favoriteFruit: String
}

struct Friend: Codable {
    let id: Int
    let name: String
}
