//
//  BecariosAPI.swift
//  Becarios
//
//  Created by Alex Tverdyy on 11/07/2019.
//  Copyright © 2019 Alex Tverdyy. All rights reserved.
//

import Foundation
import Alamofire
import CodableAlamofire

class BecariosAPI {
    
    static let shared = BecariosAPI()
    
    private let baseURL: String!
    private init() {
        self.baseURL = Constants.API_BASE
    }
    
    func getBecary(completion: @escaping (_ response: [Becario]) -> ()) {
        let url = self.baseURL + Constants.BECARIOS
        Alamofire.request(url).responseDecodableObject { (response: DataResponse<[Becario]>) in
            if let becaryArray = response.value {
                completion(becaryArray)
            }else{
                completion([])
            }
        }
    }
    
}
