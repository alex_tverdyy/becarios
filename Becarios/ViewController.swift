//
//  ViewController.swift
//  Becarios
//
//  Created by Alex Tverdyy on 11/07/2019.
//  Copyright © 2019 Alex Tverdyy. All rights reserved.
//

import UIKit
import SVProgressHUD

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var tableView: UITableView!
    
    var becaryArray: [Becario] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        loadStyles()
        loadData()
    }

    func loadStyles() {
        if let navigationBar = self.navigationController?.navigationBar {
            navigationBar.setGradientBackground(colors: [
                UIColor(red:0.07, green:0.76, blue:0.91, alpha:1.0),
                UIColor(red:0.77, green:0.44, blue:0.93, alpha:1.0),
                UIColor(red:0.96, green:0.31, blue:0.35, alpha:1.0)])
            navigationBar.topItem?.title = "Becarios"
            navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        }
    }
    
    func loadData() {
        SVProgressHUD.show()
        BecariosAPI.shared.getBecary { (becaryArray) in
            SVProgressHUD.dismiss()
            self.becaryArray.append(contentsOf: becaryArray)
            self.tableView.reloadData()
        }
    }
    
    func registerCells() {
        tableView.register(BecaryCell.self, forCellReuseIdentifier: BecaryCellIdentifier)
    }
    
    // Mark - UITableViewDataSource & UITableViewDelegate
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: BecaryCellIdentifier) as! BecaryCell
        cell.loadCell(becary: becaryArray[indexPath.row])
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 95
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return becaryArray.count
    }
    
}

