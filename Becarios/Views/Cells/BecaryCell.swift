//
//  BecaryCell.swift
//  Becarios
//
//  Created by Alex Tverdyy on 11/07/2019.
//  Copyright © 2019 Alex Tverdyy. All rights reserved.
//

import UIKit
import Kingfisher

let BecaryCellIdentifier = "BecaryCellIdentifier"

class BecaryCell: UITableViewCell {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var imageProfile: UIImageView!
    @IBOutlet weak var viewStatus: UIView!
    @IBOutlet weak var lbName: UILabel!
    @IBOutlet weak var lbBalance: UILabel!
    @IBOutlet weak var lbAddress: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        loadStyles()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func loadStyles() {
        containerView.layer.cornerRadius = 12
        containerView.layer.shadowColor = UIColor.lightGray.cgColor
        containerView.layer.shadowOffset = CGSize(width: 3, height: 3)
        containerView.layer.shadowOpacity = 0.4
        containerView.layer.shadowRadius = 3
        
        imageProfile.layer.cornerRadius = 3
        
        viewStatus.layer.cornerRadius = viewStatus.frame.size.height/2
        
        self.lbName.font = UIFont(name: "Roboto-Regular", size: 18)
        self.lbBalance.font = UIFont(name: "Roboto-Bold", size: 18)
        self.lbAddress.font = UIFont(name: "Roboto-Regular", size: 16)
    }
    
    func loadCell(becary: Becario) {
        self.imageProfile.kf.setImage(with: URL(string: becary.picture)!)
        self.viewStatus.backgroundColor = becary.isActive ? .green : .red
        self.lbName.text = becary.name
        self.lbBalance.text = becary.balance
        self.lbAddress.text = becary.address
    }

}
