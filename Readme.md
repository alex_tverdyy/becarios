# Requisitos del proyecto

- Xcode 10.2
- Cocoapods
- Conexión a internet


# Configuración basica para arrancar    

Es necesario realizar un 'pod install' en la raíz del proyecto ya que es necesario verificar que todas las dependencias estén en regla.
Una vez realizado el paso anterior el proyecto ya va se puede compilar correctamente y ejecutar en un móvil o emulador.
